#ifndef PERSONFORMS_PERSONDATA_H
#define PERSONFORMS_PERSONDATA_H

#include "app/person_data_form/types/BodyData.h"
#include "app/person_data_form/types/GeneralData.h"

struct PersonData {
    GeneralData generalData;
    BodyData bodyData;
};

#endif  // PERSONFORMS_PERSONDATA_H
