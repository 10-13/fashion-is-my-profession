//
// Created by savchik on 28/05/24.
//

#ifndef OUTFIT_MANAGER_BODYDATAREPOSITORY_H
#define OUTFIT_MANAGER_BODYDATAREPOSITORY_H

#include "core/database/abstract_repository.h"

#include "BodyDataEntity.h"

class BodyDataRepository : public outfit::core::database::AbstractRepository<BodyDataEntity> {
    static QString TableName();
};


#endif //OUTFIT_MANAGER_BODYDATAREPOSITORY_H
