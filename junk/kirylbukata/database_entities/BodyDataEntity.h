//
// Created by savchik on 28/05/24.
//

#ifndef OUTFIT_MANAGER_BODYDATAENTITY_H
#define OUTFIT_MANAGER_BODYDATAENTITY_H

#include "core/database/abstract_entity.h"
#include "app/person_data_form/types/BodyData.h"

#include <QSqlRecord>


class BodyDataEntity : public outfit::core::database::AbstractEntity<int> {

   public:
    explicit BodyDataEntity(const QSqlRecord &record);

    BodyData& GetBodyData();

   private:
    BodyData body_data_{};
};


#endif //OUTFIT_MANAGER_BODYDATAENTITY_H
