//
// Created by Pavel Kasila on 7.05.24.
//

#ifndef CREATIVE_CHART_CALLOUT_H
#define CREATIVE_CHART_CALLOUT_H

#include <QChart>
#include <QFont>
#include <QGraphicsItem>
#include <QPointF>
#include <QRectF>
#include <QString>
#include <QWidget>
#include <QtGlobal>

namespace outfit::core::statistics {
class ChartCallout : public QGraphicsItem {
   public:
    explicit ChartCallout(QChart* chart);

    void setText(const QString& text);

    void setAnchor(QPointF point);

    void updateGeometry();

    [[nodiscard]] QRectF boundingRect() const override;

    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

   protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;

   private:
    QString text_;
    QRectF text_rect_;
    QRectF rect_;
    QPointF anchor_;
    QFont font_;
    QChart* chart_;
};
}  // namespace outfit::core::statistics

#endif  // CREATIVE_CHART_CALLOUT_H
