//
// Created by Pavel Kasila on 7.05.24.
//

#ifndef CREATIVE_STATISTICS_CHART_VIEW_H
#define CREATIVE_STATISTICS_CHART_VIEW_H

#include "chart_callout.h"
#include "statistics_mode.h"

#include <QCategoryAxis>
#include <QChartView>
#include <QLineSeries>
#include <QPointF>
#include <QPolarChart>
#include <QString>
#include <QTimer>
#include <QValueAxis>
#include <memory>

namespace outfit::core::statistics {
class StatisticsChartView : public QChartView {
   public:
    explicit StatisticsChartView(StatisticsMode mode);
    ~StatisticsChartView() override;

    void update();

    void setMode(StatisticsMode mode);

    StatisticsChartView(StatisticsChartView&) = delete;
    StatisticsChartView(StatisticsChartView&&) = delete;
    StatisticsChartView& operator=(StatisticsChartView& other) = delete;
    StatisticsChartView& operator=(StatisticsChartView&& other) = delete;

   private:
    std::unique_ptr<QPolarChart> chart_;
    std::unique_ptr<QCategoryAxis> angular_axis_;
    std::unique_ptr<QLineSeries> quantity_series_;
    std::unique_ptr<QValueAxis> radial_axis_;

    std::unique_ptr<ChartCallout> tooltip_;

    std::unique_ptr<QTimer> timer_;

    StatisticsMode mode_;

    QString categoryCountQuery();

    QString countDataQuery();

    void tooltip(QPointF point, bool state);
};
}  // namespace outfit::core::statistics

#endif  // CREATIVE_STATISTICS_CHART_VIEW_H
