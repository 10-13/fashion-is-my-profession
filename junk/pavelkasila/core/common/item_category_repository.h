//
// Created by Pavel Kasila on 8.05.24.
//

#ifndef CREATIVE_ITEM_CATEGORY_REPOSITORY_H
#define CREATIVE_ITEM_CATEGORY_REPOSITORY_H

#include "item_category.h"
#include "junk/pavelkasila/core/database/abstract_repository.h"

#include <QString>

namespace outfit::core::common {
class ItemCategoryRepository : public database::AbstractRepository<ItemCategory> {
    static QString tableName();
};
}  // namespace outfit::core::common

#endif  // CREATIVE_ITEM_CATEGORY_REPOSITORY_H
