//
// Created by Pavel Kasila on 8.05.24.
//

#include "item_category_repository.h"

#include <QString>

namespace outfit::core::common {
QString ItemCategoryRepository::tableName() {
    return "item_categories";
}
}  // namespace outfit::core::common
