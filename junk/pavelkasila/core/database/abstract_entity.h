//
// Created by Pavel Kasila on 8.05.24.
//

#ifndef CREATIVE_ABSTRACT_ENTITY_H
#define CREATIVE_ABSTRACT_ENTITY_H

#include <QSqlField>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QString>
#include <QStringList>
#include <stdexcept>

namespace outfit::core::database {
template <typename ID>
class AbstractEntity {
   public:
    using Identifier = ID;

    AbstractEntity() = default;

    explicit AbstractEntity(const QSqlRecord& /*record*/) {
        throw std::runtime_error("implement record to entity");
    }

    AbstractEntity(AbstractEntity&) = default;
    AbstractEntity(AbstractEntity&&) = default;

    AbstractEntity& operator=(const AbstractEntity&) = default;
    AbstractEntity& operator=(AbstractEntity&&) = default;

    virtual ~AbstractEntity() = default;

    ID getID() {
        return id_;
    }

    void setID(ID id) {
        id_ = id;
    }

   private:
    ID id_{};

    template <typename U>
    friend class AbstractRepository;
};
}  // namespace outfit::core::database

#endif  // CREATIVE_ABSTRACT_ENTITY_H
