#ifndef RACOON_H
#define RACOON_H
#include <QGraphicsItem>
#include <QPainter>
#include <QImage>

class Racoon : public QGraphicsItem
{
private:
    QImage racoon = QImage("/home/vlad/Загрузки/racoon_main.jpg");
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

public:
    Racoon();
};

#endif // RACOON_H
