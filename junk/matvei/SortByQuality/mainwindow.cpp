#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //myRacoon = new Racoon();
    scene = new QGraphicsScene(0, 0, 400, 500);
    ui->graphicsView->setScene(scene);
    //scene->addItem(myRacoon);

    ui->graphicsView->hide();
    ui->outputBrowser->hide();
    connect(ui->sortButton, &QPushButton::clicked, this, &MainWindow::sortByParameter);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sortByParameter()
{
    QString parameter = ui -> sortEdit -> toPlainText();
    // empty exception
    if (parameter.isEmpty()) {
        ui->outputBrowser->hide();
        ui->graphicsView->show();
        QMessageBox::information(this, "Input error", "Please, make sure that the parameter field is not empty.");
        return;
    }
    ui->graphicsView->hide();
    double numericParameter = parameter.toDouble();
    // invalid input exception
    if (numericParameter <= 0 || numericParameter > 100) {
        ui->outputBrowser->hide();
        ui->graphicsView->show();
        QMessageBox::warning(this, "Input error", "Please, make sure that the parameter is a number.");
        return;
    }

    // initializing

    /*
    collection[0].setName("shorts");
    collection[0].setProbability(0.4);
    collection[0].setQuality(13);

    collection[1].setName("jeans");
    collection[1].setProbability(0.8);
    collection[1].setQuality(5);

    collection[2].setName("dress");
    collection[2].setProbability(0.9);
    collection[2].setQuality(9);

    collection[3].setName("skirt");
    collection[3].setProbability(0.45);
    collection[3].setQuality(13);

    collection[4].setName("jacket");
    collection[4].setProbability(0.73);
    collection[4].setQuality(10);

    collection[5].setName("blouse");
    collection[5].setProbability(0.8);
    collection[5].setQuality(2);

    collection[6].setName("suit");
    collection[6].setProbability(0.2);
    collection[6].setQuality(19);

    collection[7].setName("hat");
    collection[7].setProbability(0.6);
    collection[7].setQuality(7);

    collection[8].setName("pants");
    collection[8].setProbability(0.7);
    collection[8].setQuality(4);

    collection[9].setName("t-shirt");
    collection[9].setProbability(0.8);
    collection[9].setQuality(11);

    collection[10].setName("hoodie");
    collection[10].setProbability(0.5);
    collection[10].setQuality(8);

    collection[11].setName("pullover");
    collection[11].setProbability(0.6);
    collection[11].setQuality(3);

    collection[12].setName("coat");
    collection[12].setProbability(0.7);
    collection[12].setQuality(9.5);

    collection[13].setName("glasses");
    collection[13].setProbability(0.3);
    collection[13].setQuality(14);

    collection[14].setName("trainers");
    collection[14].setProbability(0.9);
    collection[14].setQuality(7);

    collection[15].setName("better to stay as it is");
    collection[15].setProbability(0.4);
    collection[15].setQuality(7);
    */

    numericParameter /= 100;

    // filter one by one
    QVector<Outfit> satisfyingCollection;
    for (size_t i = 0; i < Outfit.collection.size(); ++i) {
        if (Outfit.collection[i].getDressability() >= numericParameter) {
            satisfyingCollection.push_back(Outfit.collection[i]);
        }
    }

    // sort
    std::sort(satisfyingCollection.begin(), satisfyingCollection.end(), comp);

    QString answer = "";
    // show results
    ui->graphicsView->hide();
    ui->outputBrowser->show();
    for (size_t i = 0; i < satisfyingCollection.size(); ++i) {
        auto clothesItem = satisfyingCollection[i];
        answer += QString::number(i + 1) + ". " + clothesItem.getName() + " " + QString::number(clothesItem.getQuality()) + '\n';
    }

    ui->outputBrowser->setText(answer);
}

// dress comparator
bool MainWindow::comp(const Outfit &dressOne, const Outfit &dressTwo)
{
    return dressOne.getQuality() > dressTwo.getQuality();
}