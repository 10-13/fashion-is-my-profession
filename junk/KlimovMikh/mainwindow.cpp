#include "mainwindow.h"
#include "junk/KlimovMikh/ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    data = new PersonDataWidget(this);
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setWidget(const PersonData &d) {
    data->setPersonData(d);
}

