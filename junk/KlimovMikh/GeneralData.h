#ifndef GENERALDATA_H
#define GENERALDATA_H

#include <QString>

enum SkinColor {
    light_skin,
    dark_skin,
    default_skin
};

enum EyeColor {
    blue,
    green,
    brown,
    default_eyes
};

enum HairColor {
    light_hair,
    dark_hair,
    default_hair
};

struct GeneralData {
    int age;
    SkinColor skin_color;
    EyeColor eye_color;
    HairColor hair_color;
    QString face_shape;
    QString eye_shape;
    QString face_features;
    QString body_features;
    QString social_status;

    GeneralData(int age_input = 0, SkinColor skin_input = default_skin, EyeColor eyes_input = default_eyes,
                HairColor hair_input = default_hair, QString face_s = "", QString eye_s = "", QString face_f = "",
                QString body_f = "", QString social = "") :
        age(age_input), skin_color(skin_input), eye_color(eyes_input), hair_color(hair_input), face_shape(face_s),
        eye_shape(eye_s), face_features(face_f), body_features(body_f), social_status(social) {};
};

#endif // GENERALDATA_H
