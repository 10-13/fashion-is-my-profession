#ifndef PERSONDATA_H
#define PERSONDATA_H

#include "BodyData.h"
#include "GeneralData.h"
#include <vector>

struct PersonData {
    GeneralData generalData;
    BodyData bodyData;
    QString name;

    PersonData(GeneralData general = GeneralData(), BodyData body = BodyData(), QString in_name = "") {
        generalData = general;
        bodyData = body;
        name = in_name;
    }

    std::vector<std::string> to_text() {
        std::vector<std::string> result(22);
        result[0] = std::to_string(generalData.age);
        switch (generalData.skin_color)
            {
                case SkinColor::light_skin: result[1] = "light_skin"; break;
                case SkinColor::dark_skin: result[1] = "dark_skin"; break;
                case SkinColor::default_skin: result[1] = "default_skin"; break;
            }
        switch (generalData.eye_color)
            {
                case EyeColor::brown: result[2] = "brown"; break;
                case EyeColor::green: result[2] = "brown"; break;
                case EyeColor::blue: result[2] = "brown"; break;
                case EyeColor::default_eyes: result[2] = "default_eyes"; break;
            }
        switch (generalData.hair_color)
            {
                case HairColor::light_hair: result[3] = "light_hair"; break;
                case HairColor::dark_hair: result[3] = "dark_hair"; break;
                case HairColor::default_hair: result[3] = "default_hair"; break;
            }
        result[4] = generalData.face_shape.toStdString();
        result[5] = generalData.eye_shape.toStdString();
        result[6] = generalData.face_features.toStdString();
        result[7] = generalData.body_features.toStdString();
        result[8] = generalData.social_status.toStdString();

        result[9] = std::to_string(bodyData.weight);
        result[10] = std::to_string(bodyData.height);
        result[11] = std::to_string(bodyData.chest_girth);
        result[12] = std::to_string(bodyData.waist_girth);
        result[13] = std::to_string(bodyData.hip_girth);
        result[14] = std::to_string(bodyData.body_fat);
        result[15] = std::to_string(bodyData.muscle);
        result[16] = std::to_string(bodyData.water);
        result[17] = std::to_string(bodyData.protein);
        result[18] = std::to_string(bodyData.visceral_fat);
        result[19] = std::to_string(bodyData.basal_metabolism);
        result[20] = std::to_string(bodyData.bmi);
        switch (bodyData.weight_type)
            {
                case WeightType::UNDER_WEIGHT: result[21] = "UNDER_WEIGHT"; break;
                case WeightType::NORMAL: result[21] = "NORMAL"; break;
                case WeightType::OVER_WEIGHT: result[21] = "OVER_WEIGHT"; break;
                case WeightType::OBESE: result[21] = "OBESE"; break;
                case WeightType::DEFAULT: result[21] = "DEFAULT"; break;
            }
        return result;
    }
};

#endif // PERSONDATA_H
