#include "mainwindow.h"
#include "./ui_mainwindow.h"

namespace {
float Rule1(const float* a, const float* b) {
    return *a - *b;
}
float Rule2(const float* a, const float* b) {
    return *a;
}
float Rule3(const float* a, const float* b) {
    return *b;
}
}

Checker<float, float>* CreateChecker() {
    RuleExtendedStat<ApplyMode::Summary> Advice{std::string{"Advice"}};
    //RuleExtendedStat<ApplyMode::Multiply> Suiteble{"Suiteble"};

    auto rss = RuleStatSource<impl::DirectoConverter<float>, impl::DirectoConverter<float>, ApplyMode::Summary>{std::string{"Advice"}};
    rss.Rules.emplace_back(Rule1, "Weather", "Checks if outfit is good for event weather");
    rss.Rules.emplace_back(Rule2, "Temperature", "Checks how outfit fits outdoor temperature");
    rss.Rules.emplace_back(Rule3, "Style", "Checks if you have enough items to some event");

    auto rss2 = RuleStatSource<impl::DirectoConverter<float>, impl::DirectoConverter<float>, ApplyMode::Minimal>{std::string{"Correctness"}};
    rss2.Stat.Format = RuleFormat::Correctness;
    rss2.Rules.emplace_back(Rule2, "Test", "...");

    auto rss3 = RuleStatSource<impl::DirectoConverter<float>, impl::DirectoConverter<float>, ApplyMode::Multiply>{std::string{"Critical"}};
    rss3.Stat.Format = RuleFormat::Critical;
    rss3.Rules.emplace_back(Rule2, "Test2", "...");

    return new impl::CheckerStats<float, float, decltype(rss), decltype(rss2), decltype(rss3)>{{rss, rss2, rss3}};
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    auto a = new CheckerForm(0.9,0.1,CreateChecker(), this);
    a->setGeometry({0,0,400,800});
    this->setGeometry({0,0,400,800});
    ui->label->hide();
    a->SetState(CheckerForm::ExpandMode::Description);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::SetText(QString& Text) {
    this->ui->label->setText(Text);
}