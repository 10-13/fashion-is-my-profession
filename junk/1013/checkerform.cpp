#include "checkerform.h"
#include "ui_checkerform.h"

CheckerForm::CheckerForm(InA a, InB b,Checker<InA, InB>* checker, QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::CheckerForm)
{
    ui->setupUi(this);
    SetState(ExpandMode::Results);
    ValA_ = a;
    ValB_ = b;
    Check_ = checker;

    if(Check_ != nullptr) {
        Rules = Check_->GetRules();
        Stats = Check_->GetStats(&ValA_ , &ValB_);
    }

    UpdateStats();
}

CheckerForm::~CheckerForm()
{
    delete ui;
}

QColor CreateGradbased(float trg, float gradValue, int blue = 0, float c_low = 0, float c_max = 1) {
    auto clamp = [](float from, float to, float v) { return std::max(from,std::min(to,v));};
    int r = static_cast<int>(trg * clamp(c_low,c_max,1.5f - gradValue));
    int g = static_cast<int>(trg * clamp(c_low,c_max,2.f * gradValue - 0.5f));
    int b = blue < 0 ? std::min(r, g) / (-blue) : blue;
    return {r,g,b};
}

void CheckerForm::UpdateStats() {
    if(Check_ != nullptr) {

        Stats = Check_->GetStats(&ValA_ , &ValB_);

        ui->listWidget->clear();
        ui->listWidget_2->clear();

        for(auto i : Rules)
            ui->listWidget_2->addItem(QString::fromStdString(i->Name));
        for(auto i : Stats)
            ui->listWidget->addItem(QString::fromStdString(i->Name) + ": " + QString::fromStdString(std::to_string(i->Value)));

        constexpr int fore_rate = 140;
        constexpr int back_rate = 230;

        for(size_t i = 0; i < ui->listWidget->count(); i++) {
            ui->listWidget->item(i)->setForeground(QBrush{CreateGradbased(fore_rate, Stats[i]->Value)});
            ui->listWidget->item(i)->setBackground(QBrush{CreateGradbased(back_rate, 1.f - (float)(Stats[i]->Format) / 3.f, -1, 0.5)});
        }

        for(size_t i = 0; i < ui->listWidget_2->count(); i++) {
            ui->listWidget_2->item(i)->setToolTip(Rules[i]->Enabled ? "Enabled" : "Disabled");
            ui->listWidget_2->item(i)->setForeground(Rules[i]->Enabled ?this->palette().text() : this->palette().midlight());
        }
    }
}

void CheckerForm::SetState(ExpandMode expand_state) {
    int h = this->height();
    int w = this->width();

    const int minimize_h = 80;
    ui->groupBox->setGeometry({0,0,w - 8,expand_state == ExpandMode::Description ? h - minimize_h * 2 : minimize_h});
    ui->label->setGeometry({0,0,w - 8,expand_state == ExpandMode::Description ? h - minimize_h * 2 : minimize_h});
    ui->listWidget->setGeometry({0,(expand_state == ExpandMode::Description ? h - minimize_h * 2 : minimize_h), w - 4, expand_state == ExpandMode::Results ? (h - minimize_h * 2) : minimize_h});
    ui->listWidget_2->setGeometry({0,(expand_state == ExpandMode::Rules ? minimize_h * 2 : h - minimize_h), w, expand_state == ExpandMode::Rules ? h - minimize_h * 2 : minimize_h});

    this->updateGeometry();
}

void CheckerForm::resizeEvent(QResizeEvent *event) {
    SetState(ExpandMode::Results);
}

void LoadDescription(QLabel* label, RuleTemplate* temp) {
    QString a = "Name: ";
    a += QString::fromStdString(temp->Name);
    a += "\n    ";
    a += QString::fromStdString(temp->Description);
}

void CheckerForm::on_listWidget_clicked(const QModelIndex &index)
{
    SetState(ExpandMode::Results);
}


void CheckerForm::on_listWidget_2_clicked(const QModelIndex &index)
{
    SetState(ExpandMode::Rules);
    index.row();
}


void CheckerForm::on_listWidget_2_itemDoubleClicked(QListWidgetItem *item)
{
    RuleTemplate* trg = Rules[item->listWidget()->indexFromItem(item).row()];
    trg->Enabled = !trg->Enabled;
    UpdateStats();
}


void CheckerForm::on_listWidget_2_itemClicked(QListWidgetItem *item)
{
    RuleTemplate* trg = Rules[item->listWidget()->indexFromItem(item).row()];
    QString str = "Name: ";
    str += trg->Name;
    str += "\n    ";
    str += trg->Description;
    ui->label->setText(str);
}