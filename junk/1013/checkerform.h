#ifndef CHECKERFORM_H
#define CHECKERFORM_H

#include <QWidget>
#include <QListWidget>
#include "Checker.h"

namespace Ui {
class CheckerForm;
}


class CheckerForm : public QWidget
{
    Q_OBJECT

private:
    using InA = float;
    using InB = float;

    InA ValA_;
    InB ValB_;
    Checker<InA, InB>* Check_;

    std::vector<RuleTemplate*> Rules;
    std::vector<RuleStat*> Stats;

public:
    explicit CheckerForm(InA a, InB b,Checker<InA, InB>* checker, QWidget *parent = nullptr);
    ~CheckerForm();

    enum class ExpandMode {
        Description,
        Rules,
        Results,
    };

    void SetState(ExpandMode expand_state);

    void resizeEvent(QResizeEvent *event) override;

    void UpdateStats();

private slots:
    void on_listWidget_clicked(const QModelIndex &index);

    void on_listWidget_2_clicked(const QModelIndex &index);

    void on_listWidget_2_itemDoubleClicked(QListWidgetItem *item);

    void on_listWidget_2_itemClicked(QListWidgetItem *item);

private:
    Ui::CheckerForm *ui;
};

#endif // CHECKERFORM_H