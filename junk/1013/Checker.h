#pragma once

#include <string>
#include <vector>
#include <array>
#include <limits>
#include <tuple>

struct RuleFormat {
    enum Format {
        Recomendation = 0,
        Correctness = 1,
        Critical = 2,
    };

    constexpr static const size_t Count = 3;
    using FormatsArrayType = std::array<Format, Count>;
    constexpr static const FormatsArrayType Values { Recomendation, Correctness, Critical };
};

struct ApplyMode {
    enum Mode {
        Summary = 0,
        Multiply = 1,
        Minimal = 2,
        Maximal = 3,
    };
    constexpr static const size_t Count = 4;
    constexpr static const std::array<float, Count> InitialValues {0, 1, std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity() };
    constexpr static float& Apply(float& src, float val, Mode mode) {
        switch(mode) {
        case Summary: src += val; break;
        case Multiply: src *= val; break;
        case Minimal: src = std::min(src, val); break;
        case Maximal: src = std::max(src, val); break;
        }
        return src;
    };
    constexpr static float& Finalzie(float& src, Mode mode, size_t count) {
        switch(mode) {
        case Summary: src /= count; break;
        }
        return src;
    }
    using ApplyModesArrayType = std::array<Mode, Count>;
    constexpr static const ApplyModesArrayType Values { Summary, Multiply, Maximal, Minimal };
};

struct RuleStat {
    std::string Name;
    float Value;
    RuleFormat::Format Format{ RuleFormat::Recomendation };

    RuleStat() = delete;
    RuleStat(std::string name, float val = 0) : Name(name), Value(val) {}
};

template<ApplyMode::Mode Mode>
struct RuleExtendedStat : public RuleStat {
  RuleExtendedStat(std::string name, float val = 0) : RuleStat(name, val) {
      Value = ApplyMode::InitialValues[(int)Mode];
  }

  void Reinit() {
      Value = ApplyMode::InitialValues[(int)Mode];
  }

  void Apply(float value) {
    ApplyMode::Apply(Value, value, Mode);
  }

  void Finalzie(size_t rules) {
    ApplyMode::Finalzie(Value, Mode, rules);
  }
};

template<typename Item, typename Cond>
struct RuleSpec {
    using ItemType = Item;
    using ConditionType = Cond;

    using CPtrItemType = const Item*;
    using CPtrConditionType = const Cond*;

    using CRefItemType = const Item&;
    using CRefConditionType = const Cond&;
};

template<typename I, typename C>
using RuleChecker = float(*)(typename RuleSpec<I, C>::CPtrItemType, typename RuleSpec<I, C>::CPtrConditionType);

struct RuleTemplate {
public:
    std::string Name;
    std::string Description;
    bool Enabled{true};

    RuleTemplate() = delete;
    RuleTemplate(std::string name) : Name(name) {}
    RuleTemplate(std::string name, std::string description) : Name(name), Description(description) {}
};

template<typename Item, typename Condition>
struct Rule : public RuleTemplate {
    RuleChecker<Item, Condition> Checker;

    using cpi = RuleSpec<Item, Condition>::CPtrItemType;
    using cpc = RuleSpec<Item, Condition>::CPtrConditionType;

public:
    template<typename... Args>
    Rule(RuleChecker<Item, Condition> checker, std::string name, std::string description) : RuleTemplate(name, description), Checker(checker) {}

    float Apply(cpi item, cpc cond) {
        return Checker(item, cond);
    }
};

template<typename From, typename To>
struct ConverterBase {
    using SourceType = From;
    using ResultType = To;
};

template<typename ItemConverter, typename ConditionConverter, ApplyMode::Mode Mode>
struct RuleStatSource {
    using RuleT = Rule<typename ItemConverter::ResultType, typename ConditionConverter::ResultType>;
    using SItemType = ItemConverter::SourceType;
    using SConditionType = ConditionConverter::SourceType;

    RuleExtendedStat<Mode> Stat;
    std::vector<RuleT> Rules;

    RuleStatSource(RuleExtendedStat<Mode> stat) : Stat(stat) {}

    void Reinit() {
        Stat.Reinit();
    }
    

    void Apply(typename RuleSpec<SItemType, SConditionType>::CPtrItemType item, typename RuleSpec<SItemType, SConditionType>::CPtrConditionType cond) {
        auto its = ItemConverter::Convert(item);
        auto conds = ConditionConverter::Convert(cond);

        for(auto it : its)
            for(auto cd : conds)
                for(auto& i : Rules)
                    if(i.Enabled)
                        Stat.Apply(i.Apply(item, cond));
    }

    void Finalize() {
        size_t onRules = 0;
        for(auto& i : Rules)
            if(i.Enabled)
                onRules++;
        Stat.Finalzie(onRules);
    }
};

template<typename Item, typename Condition>
class Checker {
public:
    virtual std::vector<RuleStat*> GetStats(RuleSpec<Item, Condition>::CPtrItemType item, RuleSpec<Item, Condition>::CPtrConditionType condition) = 0;
    virtual std::vector<RuleTemplate*> GetRules() = 0;
};


namespace impl {
template<typename Item, typename Condition, typename... Stats>
class CheckerStats : public Checker<Item, Condition> {
private:
    using TupType = std::tuple<Stats...>;
    TupType Stats_;

    template<typename T>
    static int ApplyItem(RuleSpec<Item, Condition>::CPtrItemType item, RuleSpec<Item, Condition>::CPtrConditionType condition, std::vector<RuleStat*>* res, T& t) {
        t.Reinit();
        t.Apply(item, condition);
        t.Finalize();
        res->push_back(&t.Stat);
    }

    template<typename T>
    static int ApplyRule(std::vector<RuleTemplate*>* res, T& t) {
        for(auto& i : t.Rules)
            res->push_back(&i);
    }

    template<typename... Args>
    static void VOID(Args&&...) {}

public:
    CheckerStats(TupType stats) : Stats_(stats) {}

    std::vector<RuleStat*> GetStats(RuleSpec<Item, Condition>::CPtrItemType item, RuleSpec<Item, Condition>::CPtrConditionType condition) override {
        std::vector<RuleStat*> res;
        std::apply([item, condition, &res](auto&... is){
            VOID(ApplyItem(item, condition, &res, is)...);
        }, Stats_);
        return res;
    }

    std::vector<RuleTemplate*> GetRules() override {
        std::vector<RuleTemplate*> res;
        std::apply([&res](auto&... is){
            VOID(ApplyRule(&res, is)...);
        }, Stats_);
        return res;
    }
};

template<typename Type>
struct DirectoConverter : ConverterBase<Type, Type> {
    static std::array<const Type*, 1> Convert(const Type* a) {
        return {a};
    }
};
}