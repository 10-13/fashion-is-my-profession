#include "tagbar.h"
#include <QPushButton>

TagBar::TagBar(QWidget *parent, DataBase& db, int id)
    : QWidget{parent}
{
    auto tagIds = db.getTags(id);
    FlowLayout *flowLayout = new FlowLayout(this);

    for (auto& tagId: tagIds){
        auto tag = db.getTagInfo(tagId);
        QPushButton* qpb = new QPushButton(tag.first);
        qpb->setStyleSheet(buttonStyle(tag.second));

        flowLayout->addWidget(qpb);
    }

    setLayout(flowLayout);
}

QString TagBar::buttonStyle(int color){
    auto returnStyleSheet = [](QString color, QString back_color, QString press_color) {
        return (QString)"QPushButton {\
            color:" + color + ";\
            font-size: 14px;\
            font: 12pt bold \"Times New Roman\";\
            background-color: " + back_color + ";\
            border-style: solid;\
            border-width:4px;\
            border-radius:15px;\
            border-color: #444444;\
            max-height:25px;\
            min-height:25px;\
            padding: 3px;\
        }\
        \
        QPushButton:pressed {\
            font-size: 14px;\
            border-style: outset;\
            background-color: " + press_color + ";\
            border-color: #36454F;\
        }";
    };

    if (color == 0) {
        return returnStyleSheet("#FFFDD7", "#E72929", "#FF5BAE");
    } else if(color == 1){
        return returnStyleSheet("#EEEEEE", "#615EFC", "#7E8EF1");
    } else if(color == 2){
        return returnStyleSheet("#E6FF94", "#006769", "#40A578");
    } else if(color == 3){
        return returnStyleSheet("#F8F9D7", "#850F8D", "#C738BD");
    } else if(color == 4){
        return returnStyleSheet("#453F78", "#FFC94A", "#C08B5C");
    }

    return returnStyleSheet("#E0E0E0", "#666666", "#818589");
}
