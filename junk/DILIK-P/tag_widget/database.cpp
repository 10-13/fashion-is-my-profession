#include "database.h"
#include <random>

DataBase::DataBase(QObject *parent) : QObject(parent)
{
}

DataBase::~DataBase()
{
    if (db.open()) {
        db.close();
    }
}

bool DataBase::connectToDataBase()
{
    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("db.sqlite");
    if (!db.open()) {
        return false;
    }
    return true;
}

bool DataBase::createTables() {
    QString query = "CREATE TABLE IF NOT EXISTS tags ("
                    "ID integer,"
                    "NAME VARCHAR(255),"
                    "COLOR integer);";
    QSqlQuery qry;
    if (!qry.exec(query)) {
        return false;
    }
    query = "CREATE TABLE IF NOT EXISTS tags_clothes ("
            "ID integer,"
            "ID_TAG integer,"
            "ID_ITEM integer);";
    if (!qry.exec(query)) {
        return false;
    }
    return true;
}

bool DataBase::addRowTags(int id, QString name, int color) {
    QSqlQuery qry;
    qry.prepare("INSERT INTO tags ("
                "ID,"
                "NAME,"
                "COLOR)"
                "VALUES (?, ?, ?);");
    qry.addBindValue(id);
    qry.addBindValue(name);
    qry.addBindValue(color);
    if (!qry.exec()) {
        return false;
    }
    return true;
}

bool DataBase::addRowsTags(std::list<int> ids, std::list<QString> names, std::list<int> colors) {
    if (ids.size() != names.size() or names.size() != colors.size()) {
        return false;
    }
    db.transaction();
    while (!ids.empty()) {
        if (!addRowTags(ids.front(), names.front(), colors.front())) {
            db.rollback();
            return false;
        }
        ids.pop_front();
        names.pop_front();
        colors.pop_front();
    }
    db.commit();
    return true;
}

bool DataBase::addRowConnections(int id, int id_tag, int id_item) {
    QSqlQuery qry;
    qry.prepare("INSERT INTO tags_clothes ("
                "ID,"
                "ID_TAG,"
                "ID_ITEM)"
                "VALUES (?, ?, ?);");
    qry.addBindValue(id);
    qry.addBindValue(id_tag);
    qry.addBindValue(id_item);
    if (!qry.exec()) {
        return false;
    }
    return true;
}

bool DataBase::addRowsConnections(std::list<int> ids, std::list<int> ids_tags, std::list<int> ids_items) {
    if (ids.size() != ids_tags.size() or ids_tags.size() != ids_items.size()) {
        return false;
    }
    db.transaction();
    while (!ids.empty()) {
        if (!addRowConnections(ids.front(), ids_tags.front(), ids_items.front())) {
            db.rollback();
            return false;
        }
        ids.pop_front();
        ids_tags.pop_front();
        ids_items.pop_front();
    }
    db.commit();
    return true;

}

bool DataBase::deleteRow(QString table, int id) {
    QSqlQuery qry;
    if (!qry.exec("DELETE FROM " + table + " WHERE ID=" + QString::number(id) + ";")) {
        return false;
    }
    return true;
}

bool DataBase::deleteRow(QString name) {
    QSqlQuery qry;
    qry.prepare("DELETE FROM tags WHERE NAME=(?);");
    qry.addBindValue(name);
    if (!qry.exec()) {
        return false;
    }
    return true;
}

bool DataBase::clear(const QString& table) {
    QSqlQuery qry;
    if (!qry.exec("DELETE FROM " + table + ";")) {
        return false;
    }
    return true;
}

bool DataBase::clearAll() {
    QStringList tables = {"tags", "tags_clothes"};
    db.transaction();
    try {
        for (const QString& table : tables) {
            QSqlQuery query;
            if (!query.exec("DROP TABLE " + table)) {
                throw std::runtime_error("Error when deleting table " + table.toStdString() + ": " + query.lastError().text().toStdString());
            }
        }

        db.commit();
        return true;
    } catch (const std::exception& e) {
        db.rollback();
        return false;
    }
}

bool DataBase::fillTableWithRandomData(const QString& tableName, int n) {
    try{
        static const std::string characters = "abcdefghijklmnopqrstuvwxyz";
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> dis(0, 1000);
        std::list<int> vec1;
        std::list<int> vec2;
        for (int i = 0; i < n; i++) {
            vec1.push_back(dis(gen));
            vec2.push_back(dis(gen));
        }
        if (tableName == "tags") {
            std::list<QString> vec3;
            static std::uniform_int_distribution<> dis_chars(0, static_cast<int>(characters.size() - 1));
            for (int j = 0; j < n; ++j) {
                QString result;
                for (int i = 0; i < 10; ++i) {
                    result.append(characters[dis_chars(gen)]);
                }
                vec3.push_back(result);
            }
            addRowsTags(vec1, vec3, vec2);
        }
        else {
            std::list<int> vec3;
            for (int i = 0; i < n; i++) {
                vec3.push_back(dis(gen));
            }
            addRowsConnections(vec1, vec2, vec3);
        }
        return true;
    } catch (...) {
        return false;
    }

}




std::pair<QString, int> DataBase::getTagInfo(int id) {
    QSqlQuery qry;
    qry.prepare("SELECT NAME, COLOR FROM tags WHERE ID=:id");
    qry.bindValue(":id", id);
    if (qry.exec()) {
        if (qry.next()) {
            QString name = qry.value("NAME").toString();
            int color = qry.value("COLOR").toInt();
            return std::make_pair(name, color);
        }
    }
    return std::make_pair("", -1);
}

std::vector<int> DataBase::getTags(int id) {
    std::vector<int> ans;
    QSqlQuery qry;
    qry.prepare("SELECT ID_TAG FROM tags_clothes WHERE ID_ITEM=(?)");
    qry.addBindValue(id);
    if (qry.exec()) {
        while (qry.next()) {
            ans.push_back(qry.value("ID_TAG").toInt());
        }
    }
    return ans;
}
