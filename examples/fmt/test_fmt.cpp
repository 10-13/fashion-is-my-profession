#include <fmt/format.h>
#include <gtest/gtest.h>

TEST(format_test, escape) {
  EXPECT_EQ(fmt::format("{{"), "{");
  EXPECT_EQ(fmt::format("before {{"), "before {");
  EXPECT_EQ(fmt::format("{{ after"), "{ after");
  EXPECT_EQ(fmt::format("before {{ after"), "before { after");

  EXPECT_EQ(fmt::format("}}"), "}");
  EXPECT_EQ(fmt::format("before }}"), "before }");
  EXPECT_EQ(fmt::format("}} after"), "} after");
  EXPECT_EQ(fmt::format("before }} after"), "before } after");

  EXPECT_EQ(fmt::format("{{}}"), "{}");
  EXPECT_EQ(fmt::format("{{{0}}}", 42), "{42}");
}
